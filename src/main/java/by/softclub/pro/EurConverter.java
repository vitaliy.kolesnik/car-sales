package by.softclub.pro;

import java.math.BigDecimal;

public class EurConverter implements CurrencyConverter {

    private BigDecimal rate;

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    @Override
    public BigDecimal convert(BigDecimal price) {
        return price.multiply(rate);
    }
}
