package by.softclub.pro;

import java.nio.file.Files;
import java.nio.file.Paths;

public class FileLogger {

    private String fileName;

    public FileLogger(String fileName) {
        this.fileName = fileName;
    }

    public void write(String msg) throws Exception {
        // First method
//        try (PrintWriter writer = new PrintWriter(new File(fileName))) {
//            writer.println(msg);
//        }
        // Second method
        Files.writeString(Paths.get(fileName), msg);
    }

}
