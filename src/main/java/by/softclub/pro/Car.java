package by.softclub.pro;

import java.math.BigDecimal;

public enum Car {
    BMW(new BigDecimal("40000.0")),
    TOYOTA(new BigDecimal("35000.0")),
    MERCEDES(new BigDecimal("80000.0"));

    private BigDecimal price;

    Car(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
