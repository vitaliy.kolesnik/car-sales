package by.softclub.pro;

import java.math.BigDecimal;
import java.util.Map;

public class CurrencyExchange {

    private Map<String, CurrencyConverter> converters;

    public CurrencyExchange() {
    }

    public CurrencyExchange(Map<String, CurrencyConverter> converters) {
        this.converters = converters;
    }

    public Map<String, CurrencyConverter> getConverters() {
        return converters;
    }

    public void setConverters(Map<String, CurrencyConverter> converters) {
        this.converters = converters;
    }

    public BigDecimal getPrice(String currency, String car) {
        BigDecimal result = BigDecimal.ZERO;
        result = converters.get(currency).convert(Car.valueOf(car).getPrice());
//
//        if (currency.equals(Currency.USD.name())) {
//            result = converters.get("usdConverter").convert(Car.valueOf(car).getPrice());
//        } else if (currency.equals(Currency.EUR.name())) {
//            result = converters.get("eurConverter").convert(Car.valueOf(car).getPrice());
//        } else if (currency.equals(Currency.RUB.name())) {
//            result = converters.get("rubConverter").convert(Car.valueOf(car).getPrice());
//        } else {
//            System.out.println("Не верно указана валюта");
//        }
        return result;
    }

}
