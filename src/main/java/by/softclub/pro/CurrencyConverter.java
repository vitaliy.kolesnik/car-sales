package by.softclub.pro;

import java.math.BigDecimal;

public interface CurrencyConverter {

    BigDecimal convert(BigDecimal price);

}
