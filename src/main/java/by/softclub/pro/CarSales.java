package by.softclub.pro;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;

public class CarSales {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

        CurrencyExchange exchange = context.getBean("currencyExchange", CurrencyExchange.class);

        if (args.length > 1) {
            String currency = args[0].toUpperCase();
            String car = args[1].toUpperCase();
            BigDecimal price = exchange.getPrice(currency, car);
            System.out.println(price);
            if (args.length > 2 && !args[2].isEmpty()) {
                FileLogger logger = new FileLogger(args[2]);
                try {
                    logger.write(price.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("Не верно указаны параметры командной строки");
        }

    }

}
